package objetosPractica.modelo;

public class Alumno extends Persona {
	private int legajo;

	public Alumno() {}

	public Alumno(int pLegajo) {
		super();
		this.legajo = pLegajo;
	}
	

	public Alumno(String pNombre, String pApellido, int pLegajo) {
		super(pNombre, pApellido);
		legajo = pLegajo;
	}

	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int pLegajo) {
		this.legajo = pLegajo;
		
	}
	
	public boolean equals(Object obj){
		boolean bln = false;
		if(obj instanceof Alumno ){
			//downcast
			Alumno alu = (Alumno) obj;
			bln = super.equals(obj) 		&&
					alu.getLegajo()== legajo;
		}
		return bln;
	}
	public int hasthCode(){
		return super.hashCode() + legajo;
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer(super.toString());
		sb.append("\nlegajo=");
		sb.append(legajo);
		return sb.toString();
	}


	
	
	

}
