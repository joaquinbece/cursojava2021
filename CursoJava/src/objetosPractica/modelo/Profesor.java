package objetosPractica.modelo;

public class Profesor extends Persona{
	private String iosfa;

	public Profesor() {}

	public Profesor(String iosfa) {
		super();
		this.iosfa = iosfa;
	}

	public Profesor(String nombre, String apellido, String pIosfa) {
		super(nombre, apellido);
		iosfa = pIosfa;
	}

	
	public String getIosfa() {
		return iosfa;
	}
	public void setIosfa(String pIosfa) {
		this.iosfa = pIosfa;
	}

	public boolean equals(Object obj){
		boolean bln = false;
		if(obj instanceof Profesor){
			//down cast
			Profesor prof = (Profesor) obj;
			
			bln = super.equals(obj)  				&&
					prof.getIosfa() !=null 			&&
					prof.getIosfa().equals(iosfa)	;
				
		}
		return bln;
	}
	
	@Override
	public int hashCode() {		
		return super.hashCode() + iosfa.hashCode();
	}

	public String toString(){
		StringBuffer sb = new StringBuffer(super.toString());
		sb.append("\niosfa=");
		sb.append(iosfa);
		return sb.toString();
	}
	
	

}
