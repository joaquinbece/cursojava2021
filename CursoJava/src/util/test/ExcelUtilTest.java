package util.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import util.ExcelUtil;
import util.exceptions.ExcelException;

public class ExcelUtilTest {

	ExcelUtil excelUtil;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	@Before
	public void setUp(){
		List<String> encabezado = new ArrayList<String>();
		encabezado.add(new String("nombre"));
		encabezado.add(new String("Apellido"));
		encabezado.add(new String("dni"));
		
		List<String> linea1 = new ArrayList<String>();
		List<String> linea2 = new ArrayList<String>();
		List<String> linea3 = new ArrayList<String>();
		List<List<String>> filas = new ArrayList<List<String>>();
		
		linea1.add("Gabriel");
		linea1.add("Casas");
		linea1.add("22965726");
		
		linea2.add("Maria");
		linea2.add("Alvarez");
		linea2.add("22022621");
		
		linea3.add("Bel�n");
		linea3.add("Casas");
		linea3.add("40965726");
		
		filas.add(linea1);
		filas.add(linea2);
		filas.add(linea3);
		
		excelUtil = new ExcelUtil(encabezado, filas, "Prueba");
		
		
	}
	@Test
	public void testGenerarExcel() throws ExcelException, IOException {
		excelUtil.generarExcel();
	}

}
