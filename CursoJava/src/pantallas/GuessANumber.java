package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class GuessANumber {

	private JFrame frame;
	private JTextField textNumeroElegido;
	private JLabel lblArribaAbajo;
	private JLabel lblElegido;
	private JLabel lblTextoArribaAbajo;
	private JLabel lblCantIntentos;
	
	private int cantidadIntentos; //va a contar la cantidad de intentos
	private JComboBox cmbNumeroElegido;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GuessANumber window = new GuessANumber();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GuessANumber() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 655, 420);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblElijeUnNumero = new JLabel("Elije un numero");
		lblElijeUnNumero.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 30));
		lblElijeUnNumero.setBounds(204, 30, 222, 39);
		frame.getContentPane().add(lblElijeUnNumero);
		
		JLabel lblElijeUnNumero_1 = new JLabel("Elije un numero entre 1 y 99");
		lblElijeUnNumero_1.setFont(new Font("Dialog", Font.BOLD, 24));
		lblElijeUnNumero_1.setBounds(29, 136, 320, 32);
		frame.getContentPane().add(lblElijeUnNumero_1);
		
		lblCantIntentos = new JLabel("");
		lblCantIntentos.setFont(new Font("Dialog", Font.PLAIN, 16));
		lblCantIntentos.setBounds(204, 87, 46, 14);
		frame.getContentPane().add(lblCantIntentos);
		
		JLabel lbltitulo = new JLabel("El elegido es...");
		lbltitulo.setVisible(false);
		lbltitulo.setFont(new Font("Dialog", Font.PLAIN, 16));
		lbltitulo.setBounds(355, 87, 112, 21);
		frame.getContentPane().add(lbltitulo);
		
		textNumeroElegido = new JTextField();
		textNumeroElegido.setBounds(400, 147, 86, 20);
		frame.getContentPane().add(textNumeroElegido);
		textNumeroElegido.setColumns(10);
		
		lblArribaAbajo = new JLabel("");
		lblArribaAbajo.setVisible(false);
		lblArribaAbajo.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/dedoArriba_32px.png")));
		lblArribaAbajo.setBounds(183, 201, 59, 67);
		frame.getContentPane().add(lblArribaAbajo);
		
		lblTextoArribaAbajo = new JLabel("mas alto");
		lblTextoArribaAbajo.setVisible(false);
		lblTextoArribaAbajo.setBackground(Color.ORANGE);
		lblTextoArribaAbajo.setOpaque(true);
		lblTextoArribaAbajo.setFont(new Font("Dialog", Font.PLAIN, 16));
		lblTextoArribaAbajo.setBounds(29, 223, 116, 32);
		frame.getContentPane().add(lblTextoArribaAbajo);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//1- obtengo mi valor y lo guardo en un entrero
				
				//int imiValor = Integer.parseInt(textNumeroElegido.getText());
				int imiValor = Integer.parseInt((String)cmbNumeroElegido.getSelectedItem());
				
				int ivalorCompu = Integer.parseInt(lblElegido.getText());
				if(imiValor>ivalorCompu){
					lblTextoArribaAbajo.setVisible(true);
					lblTextoArribaAbajo.setText("mas abajo");
					
					lblArribaAbajo.setVisible(true);
					lblArribaAbajo.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/dedoAbajo_32px.png")));
				}else if(imiValor<ivalorCompu){
					lblTextoArribaAbajo.setVisible(true);
					lblTextoArribaAbajo.setText("mas Arriba");
					
					lblArribaAbajo.setVisible(true);
					lblArribaAbajo.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/dedoArriba_32px.png")));
				} else {
					lblTextoArribaAbajo.setVisible(true);
					lblTextoArribaAbajo.setText("Le pegaste macho...!!!");
					
					lblArribaAbajo.setVisible(true);
					lblArribaAbajo.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/correcto_32px.png")));
					lblElegido.setVisible(true);
				}
				cantidadIntentos++;
				lblCantIntentos.setText(Integer.toString(cantidadIntentos));
				
			}
		});
		btnAceptar.setBounds(522, 146, 89, 23);
		frame.getContentPane().add(btnAceptar);
		
		lblElegido = new JLabel("");
		lblElegido.setVisible(false); //solamente para probarlo
		lblElegido.setBackground(Color.GREEN);
		lblElegido.setOpaque(true);
		lblElegido.setFont(new Font("Dialog", Font.BOLD, 18));
		lblElegido.setBounds(477, 84, 40, 24);
		
		int iNumElegido = (int)(Math.random()*1000%99)+1;
		lblElegido.setText(Integer.toString(iNumElegido));
		
		frame.getContentPane().add(lblElegido);
		
		JLabel lblCantidadDeIntentos = new JLabel("Cantidad de intentos");
		lblCantidadDeIntentos.setFont(new Font("Dialog", Font.PLAIN, 16));
		lblCantidadDeIntentos.setBounds(29, 84, 144, 21);
		frame.getContentPane().add(lblCantidadDeIntentos);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cantidadIntentos = 0;
								
				lblCantIntentos.setText("");
				textNumeroElegido.setText("");
				
				lblTextoArribaAbajo.setVisible(false);
				lblArribaAbajo.setVisible(false);
			}
		});
		btnLimpiar.setBounds(522, 201, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		
		cmbNumeroElegido = new JComboBox();
		//--------------Se define el array 
		String strList[]=new String[99];//defi un array de 100 posiciones
		for(int i=0;i<99;i++)
			strList[i]=Integer.toString(i+1);
		
		cmbNumeroElegido.setModel(new DefaultComboBoxModel(strList));
		
		cmbNumeroElegido.setBounds(400, 178, 86, 20);
		frame.getContentPane().add(cmbNumeroElegido);
		
		
		
	}
}
