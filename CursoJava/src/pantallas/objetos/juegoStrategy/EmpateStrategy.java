package pantallas.objetos.juegoStrategy;

import pantallas.objetos.juegoFactory.JuegoPiedraPapelTijera;

/**
 * @author Gabriel
 * Esta clase est� vinculada con el empatae
 *
 */
public class EmpateStrategy extends ComparacionStrategy {

	@Override
	public int getResultado() {
		return juegoPiedraPapelTijera.EMPATE;
	}

	@Override
	public String getTextoResultado() {
		StringBuffer strTexto = new StringBuffer(ComparacionStrategy.juegoPiedraPapelTijera.getJugador().getAlias());
		strTexto.append(" EMPATA con ");
		strTexto.append(ComparacionStrategy.juegoPiedraPapelTijera.getContrincante().getJugador().getAlias());
		strTexto.append(" que eligieron ");
		strTexto.append(ComparacionStrategy.juegoPiedraPapelTijera.getTextoValor());
		
		return strTexto.toString();	
	}

	@Override
	public boolean isMe(JuegoPiedraPapelTijera pJuegoPpt) {
		return 	ComparacionStrategy.juegoPiedraPapelTijera.getValor() 					== 
				ComparacionStrategy.juegoPiedraPapelTijera.getContrincante().getValor()	; 	}

}
