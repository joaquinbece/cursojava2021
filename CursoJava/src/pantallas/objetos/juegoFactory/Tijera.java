package pantallas.objetos.juegoFactory;

import pantallas.objetos.juegoStrategy.ComparacionStrategy;

public class Tijera extends JuegoPiedraPapelTijera {

	public Tijera(){
		this(JuegoPiedraPapelTijera.TIJERA, "TIJERA");
	}
	public Tijera(int pValor, String pTextoValor) {
		super(pValor, pTextoValor);		
	}

	@Override
	public boolean isMe(int pConstJuego) {
		return pConstJuego == JuegoPiedraPapelTijera.TIJERA;
	}

	@Override
	public int comparar(JuegoPiedraPapelTijera pJuegoppt) {
		setContrincante(pJuegoppt);
		return ComparacionStrategy.getInstance(this).getResultado();
	}

	@Override
	public String getResultado() {
		return ComparacionStrategy.getInstance(this).getTextoResultado();
	}

}
