package pantallas.objetos.juegoFactory;

import pantallas.objetos.juegoStrategy.ComparacionStrategy;

public class Piedra extends JuegoPiedraPapelTijera {

	public Piedra() {
		super(JuegoPiedraPapelTijera.PIEDRA, "PIEDRA");
	}

	@Override
	public boolean isMe(int pConstJuego) {		
		return pConstJuego == JuegoPiedraPapelTijera.PIEDRA;
	}

	@Override
	public int comparar(JuegoPiedraPapelTijera pJuegoppt) {
		setContrincante(pJuegoppt);
		return ComparacionStrategy.getInstance(this).getResultado();
	}

	@Override
	public String getResultado() {
		return ComparacionStrategy.getInstance(this).getTextoResultado();
	}

}
